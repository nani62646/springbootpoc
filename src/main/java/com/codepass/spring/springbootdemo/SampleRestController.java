package com.codepass.spring.springbootdemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleRestController {

    @GetMapping("/getName")
    public String getName(){
        return "Ranjtih";
    }
}
