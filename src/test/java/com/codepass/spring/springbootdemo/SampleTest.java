package com.codepass.spring.springbootdemo;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.testng.annotations.Test;

import java.io.File;

import static com.codepass.spring.springbootdemo.ExtentManager.extent;

public class SampleTest {

    private static String PATH = "test-output/";
    private static String EXT = ".html";

    @Test
    public void testReport(){
        //ExtentTestManager.createTest("Sample Test case");
        String fileName = "src/test/java/com/codepass/spring/springbootdemo/test-output/testName.html";
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
        ExtentReports extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        extent.createTest("Sample Test Case").fail(new RuntimeException("RuntimeException")).assignAuthor("Vimal");
        extent.flush();
    }

    public static void main(String[] args) {
        String basePath = new File("").getAbsolutePath();
        System.out.println(basePath);

        String path = new File("src/main/resources/conf.properties")
                .getAbsolutePath();
        System.out.println(path);
    }
}
